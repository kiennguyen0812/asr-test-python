import sys
from wsgiref import headers
import _thread
import json
import websocket
import time
from read_from_mic import record
from client import on_error, on_close

HOST = "wss://dev.vinbase.ai/api/v2/asr_gpu/stream/"
LOCALHOST = "ws://localhost:2700/api/v2/asr/stream"
tokenASR = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJleHAiOjE2NjU3MTY5MTQsImlhdCI6MTYzNDE4MDkxNCwiaXNzIjoiNjA0YjJiNWFjMjdkYTMxMjE1YjlkYmViIiwicHVibGljX2lkIjoiNjA0YjJiNTNlZmZlYzZlYTE3YmQ5NTE2IiwibmFtZSI6ImFzciJ9.Qw3cBBXFsTAc37Q3hCxg2szaoAZM1JXOwOQEDDTjETzwyHbObPioDSNu2mNTeIBZ7erXafi6pjYWQZKBIVkxC1E2kCFtlkgECGDtbyOd_bbb3PBeKF8N3NugBkHTXmc1OScd-7HIh1VGdJTneosFP7TK4NVsGnFksXMOtjmHt_PJv_1ag30jXOuOHeFZrMecuNsZY4KFfxpp53pXpyMzwhA8dsm5Q5KsaQjszl7eifSNp6v48UFOj0tXyKRnB5Tv0L6oEkCBTIStzbztJrxATLOL9RtYsFaBZ-lUNsekVqu-2pGBKUvYqRJfIJjvC9tcKTsMR-E63OglEErwnhF3cQ"
header = {"token":tokenASR, "device_id":"deviceid", "session_id" :str(int(time.time())), "forward_message" :"false"}


final_transcript = []
prior_message_id = None


def print_transcript(ws, message):
    print(json.loads(message))


def talk_over_ws(ws):
    def run():
        for chunk in record():
            ws.send(chunk, opcode=websocket.ABNF.OPCODE_BINARY)

    _thread.start_new_thread(run, ())


websocket.enableTrace(True)
ws = websocket.WebSocketApp(LOCALHOST,
                            on_message=print_transcript,
                            on_error=on_error,
                            on_close=on_close,
                            header = header)
websocket.enableTrace(False)
ws.on_open = talk_over_ws
ws.run_forever()
